package com.yprodan.versereader

import android.os.Bundle
import android.util.TypedValue
import android.view.Menu
import android.view.MenuItem
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        showVerse()
    }

    private fun showVerse() {
        val filename = getString(R.string.file_name)
        val textField : TextView = findViewById(R.id.verse)
        val text = application.assets.open(filename).bufferedReader().use {
            it.readText()
        }
        textField.setText(text)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val textField : TextView = findViewById(R.id.verse)
        val textSize = textField.textSize
        when(item.itemId){
            R.id.app_bar_switch_enlarge_textSize -> {
                if(textSize < 64){
                textField.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize + 4f)}
                else{
                    Toast.makeText(this, "text size is maximum", Toast.LENGTH_SHORT).show()
                    return true
                }
            }
            R.id.app_bar_switch_reduce_textSize -> {
                if(textSize > 4) {
                    textField.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize - 4f)
                }else{
                    Toast.makeText(this, "text size is minimum", Toast.LENGTH_SHORT).show()
                    return true
                }
            }
            else -> return super.onOptionsItemSelected(item)
        }
        Toast.makeText(this, "now the text size is " + textField.textSize.toString() +
                " px", Toast.LENGTH_SHORT).show()
        return true
    }
}